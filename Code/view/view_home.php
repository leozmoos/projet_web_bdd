<?php
/**
 * Created by PhpStorm.
 * User: Eqbal.JALALY
 * Date: 13.03.2018
 * Time: 08:45
 */

$titre = "Poseidon - A propos de nous";
// ouvre la mémoire tampon
ob_start();

?>

<div id="carousel" class="carousel slide" data-ride="carousel">

    <script>
        $('.carousel').carousel({
            interval: 200
        })
    </script>

    <!-- Carousel Indicators -->
    <ul class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
    </ul>

    <!-- Carousel -->
    <div class="carousel-inner">

        <!--Text only with background image-->
        <div class="carousel-item active" style="background: url(images/cover-bg-1.jpg) center;">
            <div class="container slide-textonly">
                <div>
                    <h1>Hapy</h1>
                    <p class="lead">Collection Printemps/Eté 2018</p>
                    <a href="index.php?action=view_catalog" class="btn btn-outline-secondary">Voir les articles</a>
                </div>
            </div>
        </div>

        <!--Text with image-->
        <div class="carousel-item">
            <div class="container slide-withimage">
                <div class="description">
                    <h1>Hapy</h1>
                    <p class="lead">Collection Printemps/Eté 2018</p>
                    <a href="index.php?action=view_catalog" class="btn btn-outline-secondary">Voir les articles</a>
                </div>
                <div class="slide-image">
                    <img src="images/placeholder-shoes.png" style="width: 80%;">
                </div>
            </div>
        </div>

        <!--Text only with background image-->
        <div class="carousel-item" style="background: url(images/cover-bg-2.jpg) center;">
            <div class="container slide-textonly">
                <div>
                    <h1>Hapy</h1>
                    <p class="lead">Collection Printemps/Eté 2018</p>
                    <a href="index.php?action=view_catalog" class="btn btn-outline-secondary">Voir les articles</a>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="collections text-center ">
    <div class="container-fluid">
        <div class="row">
            <div class="collection col-md-6 alt-background">
                <div class="container container-right text-center">
                    <div>
                        <h1>Femmes</h1>
                        <p class="lead">Collection Printemps/Eté 2018</p>
                        <a href="index.php?action=view_catalog" class="btn btn-outline-secondary">Voir les articles</a>
                    </div>
                </div>
            </div>
            <div class="collection col-md-6 bg-secondary inverted">
                <div class="container container-left text-center">
                    <div>
                        <h1>Homme</h1>
                        <p class="lead">Collection Printemps/Eté 2018</p>
                        <a href="index.php?action=view_catalog" class="btn btn-outline-white">Voir les articles</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
