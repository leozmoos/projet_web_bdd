<?php
/**
 * vue pour récupérer le mot de passe
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 20.06.2018
 * Time: 14:33
 */
ob_start();
?>

<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-6">
                <section class="section register inner-left-xs">
                    <h3 class="bordered">Mot de passe oublié ?</h3>

<form role="form" class="form-group" method="post" action="index.php?action=recover_password">
    <div class="form-group">
        <table class="table">
            <tr>
                <td>
                    votre email
                    <br>
                    <br>
                    <input name="email" type="text" class="form-control">
                </td>
            </tr>

            <tr>
                <td><input type="submit" value="récupérer" class="btn btn-primary"></td>
            </tr>
        </table>
    </div>
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>



