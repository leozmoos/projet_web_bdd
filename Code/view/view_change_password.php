<?php
/**
 * Created by PhpStorm.
 * User: Benoît Meylan & Léo Zmoos
 * Date: 11.05.2018
 * Time:  22:02
 */

$titre = "Hâpy - Ajout d'article";
// ouvre la mémoire tampon
ob_start();

?>
    <main id="authentication" class="inner-bottom-md">
        <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
            <div class="row">
                <div class="col-lg-12">
                    <?php if (isset($_GET['msg']) && $_GET['msg'] == "errorold") echo "<h2><font color='red'><b>L'ancien mot de passe ne correspond pas</font></h2>"?>
                    <?php if (isset($_GET['msg']) && $_GET['msg'] == "errorconf") echo "<h2><font color='red'><b>Les deux nouveaux mot de passe ne correspondent pas</font></h2>"?>
                    <section class="section sign-in inner-right-xs">
                        <legend>Votre mot de passe</legend>
                        <form class="well form-horizontal" action="index.php?action=updatePwd" method="post">
                            <fieldset>
                                <!-- Form Name -->
                                <!-- Text input-->
                                <div class="form-group">
                                    <div class="col-md-12 inputGroupContainer">
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    <label>Ancien mot de passe</label>
                                                    <div class="input-group">
                                                        <input name="oldpwd" placeholder="Insérer votre ancien mot de passe" class="form-control" type="password" required>
                                                    </div>
                                                </td>

                                                <td>
                                                    <label>Nouveau mot de passe</label>
                                                    <div class="input-group">
                                                        <input name="newpwd" placeholder="Insérer votre nouveau mot de passe" class="form-control" type="password" required>
                                                    </div>
                                                </td>

                                                <td>
                                                    <label>Confirmation mot de passe</label>
                                                    <div class="input-group">
                                                        <input name="confnewpwd" placeholder="Confirmer votre nouveau mot de passe" class="form-control" type="password" required>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <div class="input-group">
                                                        <input value="Envoyer" class="form-control" type="submit">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </main>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>