<?php
/**
 * Created by PhpStorm.
 * User: benoit.meylan
 * Date: 11.05.2018
 * Time:  22:02
 */

$titre = "Hâpy - mon compte";
// ouvre la mémoire tampon
ob_start();

?>
<br>
<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-12">
                <section class="section sign-in inner-right-xs">
                    <div>
                        <h3>compte admin de : <?= $_SESSION['utilisateur']['prenom']." ".$_SESSION['utilisateur']['nom'] ?></h3>
                    </div>
                    <div class="vertical-menu">
                        <a href="#">Mon profil</a>
                        <a href="index.php?action=view_admin_article">Modifier/Supprimer un utilisateur</a>
                        <a href="index.php?action=view_disconnect">Se déconnecter</a>
                    </div>
                </section><!-- /.sign-in -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</main><!-- /.authentication -->
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
